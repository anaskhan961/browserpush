from flask import Flask, render_template, request
from pywebpush import webpush
import json
app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/sendNotification', methods=['GET', 'POST'])
def check():
    if request.method == 'POST':
        msg = raw_input("Enter message to send :")
        subscription = request.json['endpoint']
        keys = request.json['keys']
        webpush(
            subscription_info={
                "endpoint": subscription,
                "keys": {
                    "p256dh": keys["p256dh"],
                    "auth": keys["auth"]
                }},
            data=msg,
            vapid_private_key="private_key.pem",
            vapid_claims={
                "sub": "mailto:anas.khan96@outlook.com",
           }
        )
        return 'heyy'


if __name__ == '__main__':
    app.run(debug=True)
